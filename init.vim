if &compatible
    " `:set nocp` has many side effects. Therefore this should be done
    "" only when 'compatible' is set.
    set nocompatible
endif

if &shell =~# 'fish$'
    set shell=sh
endif

if &shell =~# 'zsh$'
    set shell=sh
endif

" Plugins
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')
    Plug 'junegunn/vim-plug'
    Plug 'editorconfig/editorconfig-vim'
    Plug 'itchyny/lightline.vim'        " A light and configurable statusline/tabline plugin for Vim
    Plug 'tpope/vim-fugitive'           " Fugitive is the premier Vim plugin for Git
    Plug 'tpope/vim-surround'           " Surround.vim is all about 'surroundings': parentheses, brackets, quotes, XML tags, and more. 
    Plug 'preservim/nerdtree'           " The NERDTree is a file system explorer for the Vim editor
    Plug 'neomake/neomake'              " Neomake is a plugin for Vim/Neovim to asynchronously run programs.
    Plug 'jeffkreeftmeijer/vim-dim'     " Dim is a clone of Vim’s default colorscheme, with some improvements
    " Languages
    Plug 'sheerun/vim-polyglot'         " A solid language pack for Vim.
    Plug 'jvirtanen/vim-hcl'            " Syntax highlighting for HashiCorp Configuration Language (HCL) used by Consul, Nomad, Packer, Terraform, and Vault.
    Plug 'numirias/semshi'              " Semshi provides semantic highlighting for Python in Neovim
    Plug 'slashmili/alchemist.vim'      " Elixir Integration Into Vim 
    Plug 'tbastos/vim-lua'              " Improved Lua 5.3 syntax and indentation support for Vim
    Plug 'stevearc/vim-arduino'         " Vim plugin for compiling and uploading arduino sketches
    "Plug 'tikhomirov/vim-glsl'          " Vim runtime files for OpenGL Shading Language
    " Initialize plugin system
call plug#end()

"if exists('+termguicolors')
"  let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
"  let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
  "  set termguicolors
"endif

augroup RestoreCursorShapeOnExit
    autocmd!
    autocmd VimLeave * set guicursor=a:ver25
augroup END

colorscheme dim

" set wildmenu                    " #
" set wildmode=list:longest,full  " #
" set updatetime=100              " #
" set sessionoptions=blank,curdir,folds,help,tabpages,winsize

"" General
set title                       " Set window title
set number                      " Show line numbers
set showbreak=⇒                 " Wrap-broken line prefix
"set textwidth=100               " Line wrap (number of cols)
set wrap linebreak nolist        " Wrap lines to fit screen
set showmatch                   " Highlight matching brace
set visualbell                  " Use visual bell (no beeping)

set hlsearch                    " Highlight all search results
set smartcase                   " Enable smart-case search
set incsearch                   " Searches for strings incrementally

set nocursorline
set autoindent                  " Auto-indent new lines
set expandtab                   " Use spaces instead of tabs
set tabstop=4                   " Number of spaces in a tab
set shiftwidth=4                " Number of auto-indent spaces
set smartindent                 " Enable smart-indent
set smarttab                    " Enable smart-tabs
set softtabstop=4               " Number of spaces per Tab

"" Advanced
set ruler                       " Show row and column ruler information
set showtabline=2               " Show tab bar

set undolevels=1000             " Number of undo levels
set backspace=indent,eol,start  " Backspace behaviour

set encoding=utf-8
set fileencoding=utf-8
set fileformat=unix
set fileformats=unix
set mouse=a
"" Highlight tabs and trailing spaces
set list listchars=tab:→\ ,space:·,trail:·,extends:⇀,precedes:↽
