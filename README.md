# My neovim configuration

My personal neovim config with plug, terminal colorscheme and lightline

## How to use it

```sh
$ git clone https://codeberg.org/n4r3n/nvim_config.git "${XDG_CONFIG_HOME:-$HOME/.config}/nvim"
$ "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/bin/init.sh"
```
