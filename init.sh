#!/usr/bin/env sh

#set -x

echo "Neovim config: ~/.config/nvim"

if [ ! -e ~/.config/nvim/autoload/plug.vim ]; then
    echo "Download vim plug"
    curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

echo "Install plugins"

nvim --headless +PlugInstall +qa

echo "Replace 'plug.vim' width symlink to plugged managed one"
rm -rf "${HOME}/.config/nvim/autoload/plug.vim"
ln -s "${HOME}/.local/share/nvim/plugged/vim-plug/plug.vim" "${HOME}/.config/nvim/autoload/plug.vim"
